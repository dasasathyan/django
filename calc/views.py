from django.shortcuts import render


# Create your views here.

def addition(request):
    a=int(request.POST['a'])
    b=int(request.POST['b'])
    operator=request.POST['op']
    if operator=='+':
        result=a+b
    elif operator=='-':
        result=a-b
    elif operator=='*':
        result=a*b
    return render(request,"result.html",{"result":result})

def form(request):
    return render(request,"form.html",{})

