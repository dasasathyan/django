from django.shortcuts import render

from average.models import students

# Create your views here.

def avg(request):
    name=request.POST['name']
    regno=int(request.POST['regno'])
    ca=int(request.POST['ca'])
    dm=int(request.POST['dm'])
    cn=int(request.POST['cn'])
    ma=int(request.POST['ma'])
    result=(ca+dm+cn+ma)/4

    average=students()
    average.sname=name
    average.regno=regno
    average.ca=ca
    average.dm=dm
    average.cn=cn
    average.ma=ma
    average.avg=result
    average.save()

    if result>75:
        progress=" distinction "
    elif 75>result>60:
        progress=" first class "
    return render(request,"avg.html",{"name":name,"avg":result,"progress":progress})

def marks(request):
    return render(request,"marks.html",{})

def display(request):
    all=students.objects.all()
    return render(request,"all.html",{"all":all})